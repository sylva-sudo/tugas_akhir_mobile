import 'package:get/get.dart';

class Dimensions {
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;
  //height
  static double pageView = screenHeight / 2.64; //320
  static double pageViewContainer = screenHeight / 3.55;
  static double pageViewTextContainer = screenHeight / 7.03; //120

  static double font16 = screenHeight / 52.75; //text exandable introduce

  // static double height10 = screenHeight / 7.81; //10
  // static double height20 = screenHeight / 40.0; //20
}
